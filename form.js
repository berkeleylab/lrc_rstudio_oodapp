'use strict'

/**
 *   Toggle the visibilty of a form group
 *  
 *   @param      {string}    form_id  The form identifier
 *   @param      {boolean}   show     Whether to show or hide
 */
function toggle_visibilty_of_form_group(form_id, show) {
  let form_element = $(form_id);
  let parent = form_element.parent();

  if(show) {
    parent.show();
  } else {
    form_element.val('');
    parent.hide();
  }
}

/**
 *  Toggle the visibilty of the SLURM queue fields
 *  
 *  interactive_mode: hidden
 *  interactive_mode_gpu: hidden
 *  batch_mode: visible
 */
function toggle_slurm_queues_field_visibility() {
  let type_ofuse = $("#batch_connect_session_context_type_ofuse");

  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_job_name',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_slurm_partition',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_bc_account',
    type_ofuse.val() === 'batch'
  );
  toggle_visibilty_of_form_group(
    '#batch_connect_session_context_qos_name',
    type_ofuse.val() === 'batch'
  );
/*  toggle_visibilty_of_form_group(
 *   '#batch_connect_session_context_bc_num_slots',
 *   type_ofuse.val() === 'batch'
 * );
 */ 
}

/**
 * Sets the change handler for the batch type of usage select.
 */
function set_type_ofuse_change_handler() {
  let type_ofuse = $("#batch_connect_session_context_type_ofuse");
  type_ofuse.change(toggle_slurm_queues_field_visibility);
}

/**
 *  Install event handlers
 */
$(document).ready(function() {
  // Ensure that fields are shown or hidden based on what was set in the last session
  toggle_slurm_queues_field_visibility();
  
  set_type_ofuse_change_handler();
});
