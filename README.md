# Batch Connect - LRC RStudio Server

A Batch Connect app that launches an RStudio server within a
batch job.

## Prerequisites

This Batch Connect app requires the following software be installed on the
**compute nodes** that the batch job is intended to run on (**NOT** the
OnDemand node):

- [RStudio](https://www.rstudio.com/)
- [R](https://www.r-project.org/)
- [Singularity](https://www.sylabs.io/docs/)

All Batch Connect apps also require the following on the compute nodes:

- [Websockify](https://pypi.org/project/websockify/)
- [TurboVNC](https://turbovnc.org)
- [nc (netcat)](http://netcat.sourceforge.net/)

**Optional** software:

- [Lmod](https://www.tacc.utexas.edu/research-development/tacc-projects/lmod)
  6.0.1+ or any other `module purge` and `module load <modules>` based CLI
  used to load appropriate environments within the batch job before launching
  the RStudio server.

## Install


These are command line only installation directions.

We start by cloning the git repository.
For production cd to the /oodapps of the LRC perceus-00 or /var/www/ood/apps/sys/ of the LRC OOD server
```sh
#Git Clone
git clone https://your_username@bitbucket.org/berkeleylab/lrc_rstudio_oodapp.git

# Change to this new directory
cd lrc_rstudio_oodapp
```

From here you will make any modifications to the code that you would like and
version your changes in your own repository:

```sh
#
# Make all your code changes while testing them in the OnDemand Dashboard
#
# ...
#

# Add the files to the Git repository
git add --all

# Commit the staged files to the Git repository

git commit -m "my changes"
```

## Contributing
1. Create your feature branch (`git checkout -b my-new-feature`)
2. Commit your changes (`git commit -am 'Add some feature'`)
3. Push to the branch (`git push origin my-new-feature`)
4. Create a new Pull Request
